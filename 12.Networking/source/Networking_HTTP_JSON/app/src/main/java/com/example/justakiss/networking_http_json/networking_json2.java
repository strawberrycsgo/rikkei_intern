package com.example.justakiss.networking_http_json;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class networking_json2 extends AppCompatActivity {
    private EditText mLoanAmount, mInterestRate, mLoanPeriod,
            mLoanAmount2, mInterestRate2, mLoanPeriod2;
    private TextView mTotalPaymentsResult;
    private String saved;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_networking_json2);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        //mBaseUrl = (EditText) findViewById(R.id.ed_1);
        mLoanAmount = (EditText) findViewById(R.id.ed_2);
        mInterestRate = (EditText) findViewById(R.id.ed_3);
        mLoanPeriod = (EditText) findViewById(R.id.ed_4);
        //mLoanAmount2 = (EditText) findViewById(R.id.ed_5);
        mInterestRate2 = (EditText) findViewById(R.id.ed_6);
        mLoanPeriod2 = (EditText) findViewById(R.id.ed_7);
        mTotalPaymentsResult = (TextView) findViewById(R.id.tw_1);
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Save the values you need from your textview into "outState"-object
        super.onSaveInstanceState(outState);
        outState.putString(saved,mTotalPaymentsResult.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mTotalPaymentsResult.setText(savedInstanceState.getString(saved));
        }
        // Read values from the "savedInstanceState"-object and put them in your textview
    }

    public void showLoanPayments2(View clickedButton) {
        String Url = "http://apps.coreservlets.com/NetworkingSupport/loan-calculator";
        //String baseUrl = mBaseUrl.getText().toString();
        String loanAmount = mLoanAmount.getText().toString();
        String interestRate = mInterestRate.getText().toString();
        String loanPeriod = mLoanPeriod.getText().toString();
        //String loanAmount2 = mLoanAmount2.getText().toString();
        String interestRate2 = mInterestRate2.getText().toString();
        String loanPeriod2 = mLoanPeriod2.getText().toString();
        LoanInputs inputs = new LoanInputs(loanAmount, interestRate, loanPeriod);
        LoanInputs inputs2 = new LoanInputs(loanAmount, interestRate2, loanPeriod2);
        JSONObject inputsJson = new JSONObject(inputs.getInputMap());
        JSONObject inputsJson2 = new JSONObject(inputs2.getInputMap());
        try {
            JSONObject jsonResult = sendPost(Url,inputsJson);
            String a = jsonResult.getString("formattedTotalPayments");
            mLoanAmount.setText(jsonResult.getString("loanAmount"));
            mInterestRate.setText(jsonResult.getString("annualInterestRateInPercent"));
            mLoanPeriod.setText(jsonResult.getString("loanPeriodInMonths"));
            JSONObject jsonResult2 = sendPost(Url,inputsJson2);
            String b = jsonResult2.getString("formattedTotalPayments");
            if(0>a.compareTo(b)) {
                mTotalPaymentsResult.setText("Option1 is has lower Total Payment:\n" + jsonResult.getString("formattedTotalPayments"));
            } else {
                mTotalPaymentsResult.setText("Option2 is has lower Total Payment:\n"  + jsonResult2.getString("formattedTotalPayments"));
            }
            mInterestRate2.setText(jsonResult2.getString("annualInterestRateInPercent"));
            mLoanPeriod2.setText(jsonResult2.getString("loanPeriodInMonths"));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
    public class LoanInputs {
        private Map<String, String> mInputMap;

        public LoanInputs(String amount, String rate, String months) {
            mInputMap = new HashMap<String, String>();
            mInputMap.put("amount", amount);
            mInputMap.put("rate", rate);
            mInputMap.put("months", months);
        }

        public Map<String, String> getInputMap() {
            return (mInputMap);
        }
    }
    private JSONObject sendPost(String Url, JSONObject jsonInputs) throws Exception {

        //Your server URL
        //String url = "http://apps.coreservlets.com/NetworkingSupport/loan-calculator";
        URL obj = new URL(Url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //Request Parameters you want to send
        String urlParameters = "loanInputs=" +jsonInputs.toString();

        // Send post request
        con.setDoOutput(true);// Should be part of code only for .Net web-services else no need for PHP
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        //Read input
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String jsonText = readAll(in);
        JSONObject json = new JSONObject(jsonText);
        return json;
    }
}
