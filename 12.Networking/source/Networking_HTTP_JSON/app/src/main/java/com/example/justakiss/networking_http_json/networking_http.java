package com.example.justakiss.networking_http_json;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class networking_http extends AppCompatActivity {
    private TextView mResultRange;
    private EditText mUrl;
    private String saved;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_networking_http);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        mResultRange=(TextView)findViewById(R.id.tw_1);
        mUrl=(EditText)findViewById(R.id.ed_url);
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Save the values you need from your textview into "outState"-object
        super.onSaveInstanceState(outState);
        outState.putString(saved,mResultRange.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mResultRange.setText(savedInstanceState.getString(saved));
        }
        // Read values from the "savedInstanceState"-object and put them in your textview
    }
    public void showResult(View clickedButton) {
        String Url =
                mUrl.getText().toString();
        LineCounter(Url);
    }
    protected void LineCounter(String urlString) {
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(urlString);
            urlConnection = (HttpURLConnection)url.openConnection();
            BufferedReader in =
                    new BufferedReader
                            (new InputStreamReader(urlConnection.getInputStream()));
            int lineNum = 0;
            //String line = in.readLine();
            //String a = String.valueOf(line!=null);
            while (in.readLine() != null) {
                lineNum++;
                in.readLine();
            }
            //Result1(a);
            Result(lineNum);
        } catch (MalformedURLException mue) {
            showError("Bad URL: " + urlString);
            mue.printStackTrace(); // View this in DDMS window
        } catch (IOException ioe) {
            showError("Error in connection: " + ioe);
            ioe.printStackTrace(); // View this in DDMS window
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }
    protected void Result(int num) {
        String introMessage =
                String.format("FOUND %s LINES!%n%n",
                        num);
        mResultRange.setText(introMessage);
    }
    protected void showError(String text) {
        mResultRange.setText("\n\n" + text);
    }
    protected void Result1(String a) {
        mResultRange.setText(a +"\n");
    }
}
