package com.example.justakiss.networking;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class Networking1 extends AppCompatActivity {
    private String mHost = "nicksosinski.com";
    private int mPort = 17;
    private TextView mResultDisplay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_networking1);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        mResultDisplay =
                (TextView)findViewById(R.id.text_display);
    }

    public void showTime(View clickedButton) {
        int k=0;
        List<String> quoteResult1 = new ArrayList<String>();
        List<String> quoteResult2 = new ArrayList<String>();
        List<String> quoteResult3 = new ArrayList<String>();
        test(quoteResult1,1);
        test(quoteResult2,2);
        test(quoteResult3,3);
    }

    private String makeOutputString(List<String> results) {
        StringBuilder output = new StringBuilder();
        for (String s: results) {
            output.append(s);
        }
        return(output.toString());
    }

    private void test(List<String> quoteResult, int k) {
        try {
            String line;
            Socket socket = new Socket(mHost, mPort);
            BufferedReader in = SocketUtils.getReader(socket);
            while(true) {
                line = in.readLine();
                if(line==null) {
                    break;
                }
                quoteResult.add(line);
            }
            String output = makeOutputString(quoteResult);

            if(k==1) {
                mResultDisplay.setText("\n"+k+"."+output+"\n");
            } else {
                mResultDisplay.append("\n"+k+"."+output+"\n");
            }
            socket.close();
        } catch (UnknownHostException uhe) {
            mResultDisplay.setText("Unknown host: " + mHost);
            uhe.printStackTrace(); // View this in DDMS window
        } catch (IOException ioe) {
            mResultDisplay.setText("IOException: " + ioe);
            ioe.printStackTrace(); // View this in DDMS window
        }
    }
    private void goToActivity
            (Class<? extends AppCompatActivity> activityClass) {
        Intent newActivity = new Intent(this, activityClass);
        startActivity(newActivity);
    }

    public void showNetworking2(View clickedButton) {
        goToActivity(Networking2.class);
    }
}

