package com.example.justakiss.networking;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class Networking2 extends AppCompatActivity {
    private EditText mEditTextHost;
    private int mPort = 43;
    private String mHost ="whois.verisign-grs.com";
    private TextView mResultDisplay;
    private String rotated;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_networking2);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        mResultDisplay = (TextView)findViewById(R.id.text_display2);
        mEditTextHost = (EditText) findViewById(R.id.et_1);

    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Save the values you need from your textview into "outState"-object
        super.onSaveInstanceState(outState);
        outState.putString(rotated,mResultDisplay.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState!=null) {
            mResultDisplay.setText(savedInstanceState.getString(rotated));
        }
        // Read values from the "savedInstanceState"-object and put them in your textview
    }
    public void showTime(View clickedButton) {
        test2();
    }
    private void goToActivity
            (Class<? extends AppCompatActivity> activityClass) {
        Intent newActivity = new Intent(this, activityClass);
        startActivity(newActivity);
    }

    public void showNetworking1(View clickedButton) {
        goToActivity(Networking1.class);
    }
    private void test2() {
        try {
            String host = mEditTextHost.getText().toString();
            Socket socket = new Socket(mHost, mPort);
            PrintWriter out = SocketUtils.getWriter(socket);
            out.println(host);
            BufferedReader in = SocketUtils.getReader(socket);
            String line = in.readLine();
            while(!line.endsWith("detailed information.")) {
                line = in.readLine();
            }
            line = in.readLine();
            mResultDisplay.setText("");
            while(!line.endsWith("<<<")){
                mResultDisplay.append(line + "\n");
                line = in.readLine();
            }
            socket.close();
        } catch (UnknownHostException uhe) {
            mResultDisplay.setText("Unknown host: " + mHost);
            uhe.printStackTrace(); // View this in DDMS window
        } catch (IOException ioe) {
            mResultDisplay.setText("IOException: " + ioe);
            ioe.printStackTrace(); // View this in DDMS window
        }
    }
    private void test1() {
        try {
            String host = mEditTextHost.getText().toString();
            Socket socket = new Socket(mHost, mPort);
            PrintWriter out = SocketUtils.getWriter(socket);
            out.println(host);
            BufferedReader in = SocketUtils.getReader(socket);
            String line = in.readLine();
            line=in.readLine();
            String line1;
            while(!line.endsWith("<<<")){
                line1= String.valueOf(line.startsWith("Domain Name:"));
                mResultDisplay.append(line + "\n");
                mResultDisplay.append(line1 + "\n");
                line = in.readLine();
            }
            socket.close();
        } catch (UnknownHostException uhe) {
            mResultDisplay.setText("Unknown host: " + mHost);
            uhe.printStackTrace(); // View this in DDMS window
        } catch (IOException ioe) {
            mResultDisplay.setText("IOException: " + ioe);
            ioe.printStackTrace(); // View this in DDMS window
        }
    }
}

