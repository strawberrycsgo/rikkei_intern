package com.example.justakiss.rotationtest;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView mTextRegion;
    private EditText mEditedText;
    String tv_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextRegion= (TextView) findViewById(R.id.tv_1);
        mEditedText= (EditText) findViewById(R.id.et_1);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Save the values you need from your textview into "outState"-object
        super.onSaveInstanceState(outState);
        outState.putString(tv_text,mTextRegion.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState!=null) {
         mTextRegion.setText(savedInstanceState.getString(tv_text));
        }
        // Read values from the "savedInstanceState"-object and put them in your textview
    }

    public void AddToTextView (View ClickedButton) {
        mTextRegion.setText(mEditedText.getText().toString());
    }

}
