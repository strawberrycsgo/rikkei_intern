package com.example.justakiss.uiwidget_c2;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ToggleButton;

public class Button extends AppCompatActivity {
    private View mColorRegion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button);
        mColorRegion=findViewById(R.id.color_text_view);
        ImageButton ib1 = (ImageButton) findViewById(R.id.image_but_red);
        ImageButton ib2 = (ImageButton) findViewById(R.id.image_but_blue);
        RadioButton rb1 = (RadioButton) findViewById(R.id.radio_but_red);
        RadioButton rb2 = (RadioButton) findViewById(R.id.radio_but_blue);
        RadioButton rb3 = (RadioButton) findViewById(R.id.radio_but_yellow);

        ib1.setOnClickListener(new setColorTextView(Color.RED));
        ib2.setOnClickListener(new setColorTextView(Color.BLUE));
        rb1.setOnClickListener(new setColorTextView(Color.RED));
        rb2.setOnClickListener(new setColorTextView(Color.BLUE));
        rb3.setOnClickListener(new setColorTextView(Color.YELLOW));
    }

    private void ColorSetter(int color) {
        mColorRegion.setBackgroundColor(color);
    }

    private class setColorTextView implements View.OnClickListener {
        private int mColor;
        public setColorTextView(int mColor) {
            this.mColor=mColor;
        }
        @Override
        public void onClick (View v) {
            ColorSetter(mColor);
        }
    }
    public void showToggleButtonInfo (View toggledButton) {
        ToggleButton toggleButton=(ToggleButton)toggledButton;
        if(toggleButton.isChecked()) {
           if(toggleButton.getId()==R.id.toggle_red) {
               ColorSetter(Color.RED);
           } else if(toggleButton.getId()==R.id.toggle_blue) {
               ColorSetter(Color.BLUE);
           } else if(toggleButton.getId()==R.id.toggle_yellow) {
               ColorSetter(Color.YELLOW);
           }
        } else {
            ColorSetter(Color.BLACK);
        }
    }
    public void RadioGroupSetColor (View ClickedButton) {
        RadioGroup rg1 = (RadioGroup) findViewById(R.id.radio_group_1);
        int butId = rg1.getCheckedRadioButtonId();
        switch (butId) {
            case R.id.radio_but_red2:
                ColorSetter(Color.RED);
                break;
            case R.id.radio_but_blue2:
                ColorSetter(Color.BLUE);
                break;
            case R.id.radio_but_yellow2:
                ColorSetter(Color.YELLOW);
                break;
        }
    }
}
