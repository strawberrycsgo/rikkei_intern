package com.example.justakiss.spinnersex;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Spinners extends AppCompatActivity {
    private String mItemSelectedMessageTemplate;
    private View mColorRegion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);
        mItemSelectedMessageTemplate = getString(R.string.spinner_message_template);
        Spinner spinner1 = (Spinner)findViewById(R.id.spinner1);
        Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
        mColorRegion=findViewById(R.id.color_text);
        spinner1.setOnItemSelectedListener(new SpinnerInfo());
        List<String> futureAndroidVendors = getFutureAndroidVendors();
        ArrayAdapter<String> spinner2Adapter =
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item,
                        futureAndroidVendors);
        spinner2Adapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(spinner2Adapter);
        spinner2.setOnItemSelectedListener(new SpinnerInfo());
    }

    private List<String> getFutureAndroidVendors() {
        String[] vendorArray = { "Red", "Blue",
                "Yellow", "Green" };
        List<String> vendorList = Arrays.asList(vendorArray);
        Collections.shuffle(vendorList);
        return(vendorList);
    }

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    private void ColorSetter (int color) {
        mColorRegion.setBackgroundColor(color);
    }

    private class SpinnerInfo implements AdapterView.OnItemSelectedListener {
        private boolean isFirst = true;
        @Override
        public void onItemSelected(AdapterView<?> spinner, View selectedView,
                                   int selectedIndex, long id) {
            String text =spinner.getItemAtPosition(selectedIndex).toString();
            switch (text) {
                case "Red":
                    ColorSetter(Color.RED);
                    break;
                case "Blue":
                    ColorSetter(Color.BLUE);
                    break;
                case "Yellow":
                    ColorSetter(Color.YELLOW);
                    break;
                case "Green":
                    ColorSetter(Color.GREEN);
                    break;
            }
            if (isFirst) {
                isFirst = false;
            } else {
                String selection =
                        spinner.getItemAtPosition(selectedIndex).toString();
                String message =
                        String.format(mItemSelectedMessageTemplate, selection);
                showToast(message);
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> spinner) {
        }
    }
}
