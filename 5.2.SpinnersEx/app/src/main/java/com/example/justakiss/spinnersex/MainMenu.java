package com.example.justakiss.spinnersex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
    }
    private void goToActivity
            (Class<? extends AppCompatActivity> activityClass) {
        Intent newActivity = new Intent(this, activityClass);
        startActivity(newActivity);
    }

    public void showSpinners(View clickedButton) {
        goToActivity(Spinners.class);
    }

    public void showAct2(View clickedButton) {
        goToActivity(Act2.class);
    }
}
