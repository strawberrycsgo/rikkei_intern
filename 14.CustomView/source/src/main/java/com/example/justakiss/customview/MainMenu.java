package com.example.justakiss.customview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void goToRedView (View ClickedButton) {
        Intent toRedViewIntent = new Intent(this, RedView1.class);
        startActivity(toRedViewIntent);
    }
    public void goToColouredView (View ClickedButton) {
        Intent toColouredView = new Intent(this, ColouredView1.class);
        startActivity(toColouredView);
    }
}
