package com.example.justakiss.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class RedView extends View {
    public static int mDesiredDimen = 200;
    private int mColor = Color.RED;
    public static int mPressedColor = Color.CYAN;
    public RedView(Context context) {
        super(context);
    }
    public RedView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    @Override
    public boolean onTouchEvent (MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mColor = mPressedColor;
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                mColor = Color.RED;
                invalidate();
                break;
        }

        return true;
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//        int viewWidth = getWidth();
//        int viewHeight = getHeight();
//        canvas.translate(viewWidth, viewHeight);
        canvas.drawColor(mColor);
    }
    @Override
    protected void onMeasure(int widthMeasureSpec,
                             int heightMeasureSpec) {
        int width = measureWidth(widthMeasureSpec);
        int height = measureHeight(heightMeasureSpec);
        setMeasuredDimension(width, height);
    }
    private int measureWidth(int measureSpec) {
        return(measureText(measureSpec, getPaddingLeft(),
                getPaddingRight()));
    }
    private int measureHeight(int measureSpec) {
        return(measureText(measureSpec, getPaddingTop(),
                getPaddingBottom()));
    }
    private int measureText(int measureSpec,
                            int padding1, int padding2) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = mDesiredDimen + padding1 + padding2;
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return(result);
    }
}

