package com.example.justakiss.customview;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

/**
 * Created by justakiss on 27/07/2016.
 */
public class ColouredView1 extends AppCompatActivity {
    //public int mDefaultTextSize;
    private EditText mEditTextValue2;
    private ColouredView mColouredViewField;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colouredview);
        mEditTextValue2 = (EditText) findViewById(R.id.et_2);
        mColouredViewField = (ColouredView) findViewById(R.id.colouredview1);
    }
    public void blueButton1 (View ClickedButton) {
        ColouredView.mPressedColor = this.getResources().getColor(R.color.colorBlue);
    }
    public void yellowButton1 (View ClickedButton) {
        ColouredView.mPressedColor = this.getResources().getColor(R.color.colorYellow);
    }
    public void greenButton1 (View ClickedButton) {
        ColouredView.mPressedColor = this.getResources().getColor(R.color.colorGreen);
    }
    public void redButton1 (View ClickedButton) {
        ColouredView.mPressedColor = this.getResources().getColor(R.color.colorRed);
    }
    public void goButtonClicked (View ClickedButton) {
        if(mEditTextValue2.getText().length() != 0) {
            ColouredView.mDesiredDimen = Integer.parseInt(mEditTextValue2.getText().toString());
            //mColouredViewField.setBackgroundColor(ColouredView.mDesiredDimen);
            mColouredViewField.requestLayout();
        }
    }
}
