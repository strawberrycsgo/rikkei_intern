package com.example.justakiss.customview;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

/**
 * Created by justakiss on 27/07/2016.
 */
public class RedView1 extends AppCompatActivity {
    private EditText mEditTextValue;
    private RedView mRedViewField;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redview);
        mEditTextValue = (EditText) findViewById(R.id.et_1);
        mRedViewField = (RedView) findViewById(R.id.redview1);
    }
    public void blueButton (View ClickedButton) {
        RedView.mPressedColor = this.getResources().getColor(R.color.colorBlue);
        //RedView.mPressedColor = Color.BLUE;
    }
    public void yellowButton (View ClickedButton) {
        RedView.mPressedColor = this.getResources().getColor(R.color.colorYellow);
    }
    public void greenButton (View ClickedButton) {
        RedView.mPressedColor = this.getResources().getColor(R.color.colorGreen);
    }
    public void goButtonClicked (View ClickedButton) {
        if(mEditTextValue.getText().length() != 0) {
            RedView.mDesiredDimen = Integer.parseInt(mEditTextValue.getText().toString());
            mRedViewField.requestLayout();
        }
    }
}
