package com.example.justakiss.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class ColouredView extends View {
    public static int mDesiredDimen = 200;
//    public static int mDesiredDimen2 = 0;
    private int mDefaultColor = Color.RED;
    private int mColor;
    public static int mPressedColor = Color.CYAN;
    public ColouredView(Context context) {
        super(context);
    }
    public ColouredView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeCustomAttributes(attrs);
    }
    private void initializeCustomAttributes(AttributeSet attrs) {
        TypedArray attributeArray =
                getContext().obtainStyledAttributes(attrs,
                        R.styleable.ColouredView);
        mDefaultColor =
                attributeArray.getInt(R.styleable.ColouredView_default_color,
                        mDefaultColor);
        mPressedColor =
                attributeArray.getInt(R.styleable.ColouredView_default_pressed_color,
                        mPressedColor);
        mDesiredDimen = attributeArray.getDimensionPixelSize(R.styleable.ColouredView_desired_dimension,mDesiredDimen);
//        if (mDesiredDimen < 0) {
//            mDesiredDimen = 200;
//        }
        mColor=mDefaultColor;
//        if(mDesiredDimen2 != 0) {
//            mDesiredDimen = mDesiredDimen2;
//        }
    }
    @Override
    public boolean onTouchEvent (MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mColor = mPressedColor;
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                mColor = mDefaultColor;
                invalidate();
                break;
        }
        return true;
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int viewWidth = getWidth();
        int viewHeight = getHeight();
        canvas.translate(viewWidth, viewHeight);
        canvas.drawColor(mColor);
    }
    @Override
    protected void onMeasure(int widthMeasureSpec,
                             int heightMeasureSpec) {
        int width = measureWidth(widthMeasureSpec);
        int height = measureHeight(heightMeasureSpec);
        setMeasuredDimension(width, height);
    }
    private int measureWidth(int measureSpec) {
        return(measureText(measureSpec, getPaddingLeft(),
                getPaddingRight()));
    }
    private int measureHeight(int measureSpec) {
        return(measureText(measureSpec, getPaddingTop(),
                getPaddingBottom()));
    }
    private int measureText(int measureSpec,
                            int padding1, int padding2) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = mDesiredDimen + padding1 + padding2;
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return(result);
    }
}

