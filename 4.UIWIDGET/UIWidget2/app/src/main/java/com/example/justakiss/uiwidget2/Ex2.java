package com.example.justakiss.uiwidget2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class Ex2 extends AppCompatActivity {
    private TextView mTextRegion;
    private String mTempMes;
    String text1="Button1 pressed";
    String text2="Button2 pressed";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex2);
        mTextRegion=(TextView) findViewById(R.id.text_region);
        mTempMes=getString(R.string.temp_mes);
        Button b1 = (Button) findViewById(R.id.button1);
        Button b2 = (Button) findViewById(R.id.button2);
        b1.setOnClickListener(new myOnClickListener(R.string.mesHello,this));
        b2.setOnClickListener(new myOnClickListener(R.string.mesGoodbye,this));
    }

    public void InsertToTextView (int butId) {
        String text=getString(butId);
        mTextRegion.setText(text);
    }

    public void ShowInfo (int buttonId) {
        String name=getString(buttonId);
        //String message=String.format(mTempMes, name);
        Toast temp=Toast.makeText(this,name,Toast.LENGTH_LONG);
        temp.show();
    }
}

