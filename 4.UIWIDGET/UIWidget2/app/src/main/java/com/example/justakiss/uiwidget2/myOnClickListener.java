package com.example.justakiss.uiwidget2;

import android.view.View;

public class myOnClickListener implements View.OnClickListener {
    private int mTemp;
    private Ex2 mainAct;
    public myOnClickListener(int mTemp, Ex2 mainAct) {
        this.mTemp = mTemp;
        this.mainAct=mainAct;
    }

    @Override
    public void onClick(View v) {
        //int butId = v.getId();
        mainAct.InsertToTextView(mTemp);
        mainAct.ShowInfo(mTemp);
    }
}
