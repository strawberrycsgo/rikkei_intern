package com.example.justakiss.mynote;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import java.util.ArrayList;
/**
 * Created by justakiss on 22/08/2016.
 * Adapter for main menu note gridview
 */
public class GridViewCustomAdapter extends BaseAdapter {
    private Context context;
    private String[] textViewValues;
    private String[] textViewValues2;
    private String[] textViewValues3;
    private String[] isAlarm;
    private ArrayList<Integer> idNote;
    private Integer length;
    private Integer[] mColor;

    public GridViewCustomAdapter(Context c){
        context = c;
    }
    public GridViewCustomAdapter(Context context, String[] textViewValues,
                                 String[] textViewValues2, String[] textViewValues3,
                                 ArrayList<Integer> idNote, Integer [] color, String[] isAlarm) {
        this.context = context;
        this.textViewValues = textViewValues;
        this.textViewValues2 = textViewValues2;
        this.textViewValues3 = textViewValues3;
        this.idNote = idNote;
        this.mColor = color;
        this.isAlarm = isAlarm;
//        this.length = length;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
//        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;
        if (convertView == null) {
            gridView = new View(context);
            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.item, null);
            // set value into textview
        } else {
            gridView = (View) convertView;
        }
        TextView name = (TextView) gridView.findViewById(R.id.gw_name);
        TextView date = (TextView) gridView.findViewById(R.id.gw_time);
        TextView detail = (TextView) gridView.findViewById(R.id.gw_detail);
        ImageButton alarm = (ImageButton) gridView.findViewById(R.id.gw_alarm);
        LinearLayout bground = (LinearLayout) gridView.findViewById(R.id.gw_item);
        bground.setBackgroundColor(mColor[position]);
        name.setText(textViewValues[position]);
        detail.setText(textViewValues2[position]);
        date.setText(textViewValues3[position]);
        if (isAlarm[position].matches("no")) {
            alarm.setImageResource(android.R.color.transparent);
        } else {
            alarm.setImageResource(R.drawable.ic_alarm);
        }
        gridView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Sending image id to FullScreenActivity
                Intent i = new Intent(context, CurrNote.class);
                // passing array index
                i.putExtra("id", idNote);
                i.putExtra("pos", position);
                i.putExtra("max", textViewValues.length);
                context.startActivity(i);
            }
        });
        return gridView;
    }
    @Override
    public int getCount() {
        return textViewValues.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}