package com.example.justakiss.mynote;

import android.graphics.Color;

import java.util.ArrayList;

/**
 * Created by justakiss on 01/08/2016.
 * Note class for data store
 */
public class Note {
    //private variables
    int _id;
    String _name;
    String _detail;
    String _date;
    String _alarmdate;
    String _alarmtime;
    Integer _color = Color.WHITE;
    ArrayList<String> _photo;

    // Empty constructor
    public Note(){

    }
    // constructor
    public Note(int id, String name, String detail, String date, String alarmdate,
                String alarmtime, ArrayList<String> photo, Integer color){
        this._id = id;
        this._name = name;
        this._detail = detail;
        this._date = date;
        this._alarmdate = alarmdate;
        this._alarmtime = alarmtime;
        this._photo = photo;
        this._color = color;
    }
    public Note(String name, String detail, String date, String alarmdate,
                String alarmtime, ArrayList<String> photo, Integer color){
        this._name = name;
        this._detail = detail;
        this._date = date;
        this._alarmdate = alarmdate;
        this._alarmtime = alarmtime;
        this._photo = photo;
        this._color = color;
    }
    public Note(int id, String name, String detail, String date){
        this._id = id;
        this._name = name;
        this._detail = detail;
        this._date = date;
        this._alarmdate = null;
        this._alarmtime = null;
        this._photo = null;
    }

    // constructor
    public Note(String name, String detail, String date){
        this._name = name;
        this._detail = detail;
        this._date = date;
        this._alarmdate = null;
        this._alarmtime = null;
        this._photo = null;
    }
    // getting ID
    public int getID(){
        return this._id;
    }
    // setting id
    public void setID(int id){
        this._id = id;
    }

    // getting name
    public String getName(){
        return this._name;
    }
    // setting name
    public void setName(String name){
        this._name = name;
    }

    // getting detail
    public String getDetail(){
        return this._detail;
    }
    // setting detail
    public void setDetail(String detail){
        this._detail = detail;
    }

    //getting date
    public String getDate(){
        return this._date;
    }
    // setting date
    public void setDate(String date){
        this._date = date;
    }

    //getting alarmdate
    public String getAlarmDate(){
        return this._alarmdate;
    }
    //setting alarmdate
    public void setAlarmDate(String alarmdate){
        this._alarmdate = alarmdate;
    }

    //getting alarmtime
    public String getAlarmTime(){
        return this._alarmtime;
    }
    //setting alarmtime
    public void setAlarmTime(String alarmtime){
        this._alarmtime = alarmtime;
    }

    //getting photo
    public ArrayList<String> getPhoto(){
        return this._photo;
    }
    //setting photo
    public void setPhoto(ArrayList<String> photo){
        this._photo = photo;
    }

    // getting color
    public Integer getColor(){
        return this._color;
    }
    // setting color
    public void setColor(int color){
        this._color = color;
    }
}
