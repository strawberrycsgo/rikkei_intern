package com.example.justakiss.mynote;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.LinearLayout;

public class FullScreenViewActivity extends FragmentActivity {
    private ViewPager mViewPager;
    private DBHandler mDB;
    Note note;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
        mDB = new DBHandler(getApplicationContext());
        Intent i = getIntent();
        String pos = i.getExtras().getString("url");
//        note = mDB.getNote(pos);
//        String[] path = note.getPhoto();
        CustomPagerAdapter mCustomPagerAdapter = new CustomPagerAdapter(this,pos);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mCustomPagerAdapter);
    }
}
//public class FullScreenViewActivity extends AppCompatActivity {
//    private String[] FilePathStrings;
//    private String[] FileNameStrings;
//    private File[] listFile;
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_fullscreen);
//        Intent i = getIntent();
//        String pos = i.getExtras().getString("url");
//        String imageUrl = pos;
//        File file = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
//        if (file.isDirectory()) {
//            listFile = file.listFiles();
//            // Create a String array for FilePathStrings
//            FilePathStrings = new String[listFile.length];
//            // Create a String array for FileNameStrings
//            FileNameStrings = new String[listFile.length];
//
//            for (int k = 0; k < listFile.length; k++) {
//                // Get the path of the image file
//                FilePathStrings[k] = listFile[k].getAbsolutePath();
//                // Get the name image file
//                FileNameStrings[k] = listFile[k].getName();
//            }
//        }
////        WebView wv = (WebView) findViewById(R.id.yourwebview);
////        wv.getSettings().setBuiltInZoomControls(true);
////        wv.loadUrl(imageUrl);
//        FullScreenImageAdapter fullScreenAdap = new FullScreenImageAdapter(this,FilePathStrings,FileNameStrings);
//    }
//}
