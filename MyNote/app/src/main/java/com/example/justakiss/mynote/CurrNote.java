package com.example.justakiss.mynote;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.pes.androidmaterialcolorpickerdialog.ColorPicker;

import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by justakiss on 03/08/2016.
 */
public class CurrNote extends AppCompatActivity {
    public static int sDeleted = 0;
    public static String [] sPath = new String [100];
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int PICK_IMAGE = 2;
    private boolean colorChanged = false;
    private int mColor = Color.WHITE;
    private View mNoteBackground;
    private String mCurrentPhotoPath, nextDay,
            day = null, time = null;
    private TextView mTime, mToolbarTitle, mTv1;
    private EditText mName, mDetail;
    private DBHandler mDB;
    private UnscrollableGridView mGridView;
    private boolean isAlarm = false, wasAlarm = false;
    private ArrayList<String> imgPath;
    private int mode = 2, mNoteId, mMaxId,
            mPos, countImage = 0, mThisId;
    private ArrayList<Integer> mAllId = new ArrayList<Integer>();
    private int mYear,mMonth,mDay,mHour,mMinute;
    private Spinner mSp1, mSp2;
    private GridImgAdap gridImgAdap;
    private ImageView mIb1;
    private List<String> spinnerDate, spinnerTime;
    private Context mContext;
    private ArrayAdapter<String> spinner1Adapter, spinner2Adapter;
    private DialogFragment newFragment, newFragment2;
    private Toolbar bottomToolbar;
    private Note currNote;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_curr_note);
        //Create toolbar
        Toolbar noteToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        bottomToolbar = (Toolbar) findViewById(R.id.my_toolbar_bottom);
        setSupportActionBar(noteToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //Create space for current note info
        Intent i = getIntent();
        mAllId = i.getExtras().getIntegerArrayList("id"); //List of all notes id
        mPos = i.getExtras().getInt("pos"); //Note's current position in table
        mMaxId = i.getExtras().getInt("max"); //Number of notes
        setupInfo();
        setupToolbar();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK || requestCode == PICK_IMAGE) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                imgPath.add(mCurrentPhotoPath);
                countImage++;
                if(sDeleted!=0) {
                    for(int i=0;i<sDeleted;i++) {
                        imgPath.remove(sPath[i]);
                    }
                }
            }
            if (requestCode == PICK_IMAGE) {
                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                imgPath.add(picturePath);
                countImage++;
                if(sDeleted!=0) {
                    for(int i=0;i<sDeleted;i++) {
                        imgPath.remove(sPath[i]);
                    }
                }
            }
            gridImgAdap = new GridImgAdap(this,imgPath,mode);
            mGridView.setAdapter(gridImgAdap);
        }
    }
    @Override
    public void onBackPressed() {
        try {
            changeNote();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Intent intent2 = new Intent(this,MainActivity.class);
        this.startActivity(intent2);
        overridePendingTransition(0, 0);
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu3, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                try {
                    changeNote();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                this.startActivity(new Intent(this,MainActivity.class));
                overridePendingTransition(0, 0);
                finish();
                return true;
            case R.id.action_add_photo:
                showInsertPictureDialog();
                return true;
            case R.id.action_confirm:
                try {
                    changeNote();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                this.startActivity(new Intent(this,MainActivity.class));
                overridePendingTransition(0, 0);
                finish();
                return true;
            case R.id.action_choose_color:
                showChooseColorDialog();
                return true;
            case R.id.action_settings:
                try {
                    changeNote();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                this.startActivity(new Intent(this,NewNote.class));
                overridePendingTransition(0, 0);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void setupInfo() {
        mTv1 = (TextView) findViewById(R.id.tw_sp_curr_note);
        mSp1 = (Spinner) findViewById(R.id.sp_date_curr_note);
        mSp2 = (Spinner) findViewById(R.id.sp_time_curr_note);
        mIb1 = (ImageButton) findViewById(R.id.ib_close_curr);
        mDB = new DBHandler(getApplicationContext());
        mTime = (TextView) findViewById(R.id.tw_curr_note_time);
        mName = (EditText) findViewById(R.id.et_curr_note_title);
        mDetail = (EditText) findViewById(R.id.et_curr_note_detail);
        mGridView = (UnscrollableGridView) findViewById(R.id.curr_grid);
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mNoteBackground = findViewById(R.id.curr_note_id);
        isNoTitle(); //if note don't has title use default title
        mContext = this;
        //Get info from adapter
//        Intent i = getIntent();
//        mAllId = i.getExtras().getIntegerArrayList("id"); //List of all notes id
//        mPos = i.getExtras().getInt("pos"); //Note's current position in table
//        mMaxId = i.getExtras().getInt("max"); //Number of notes
        mNoteId = mAllId.get(mMaxId - mPos - 1); //Note's current id
        try {
            currNote = mDB.getNote(mNoteId);
            if(currNote==null) {
                Toast.makeText(this,"This Note has been deleted!",Toast.LENGTH_LONG).show();
                Intent intent2 = new Intent(this,MainActivity.class);
                this.startActivity(intent2);
                overridePendingTransition(0, 0);
                finish();
            } else {
                //Show note info
                mName.setText(currNote.getName());
                mDetail.setText(currNote.getDetail());
                mTime.setText(currNote.getDate());
                mToolbarTitle.setText(currNote.getName());
                imgPath = currNote.getPhoto();
                Log.d(String.valueOf(imgPath),"imgpath\n");
                mThisId=currNote.getID();
                mNoteBackground.setBackgroundColor(currNote.getColor());
                mColor=currNote.getColor();
                gridImgAdap = new GridImgAdap(this,imgPath,mode);
                mGridView.setAdapter(gridImgAdap);
                day = currNote.getAlarmDate();
                time = currNote.getAlarmTime();
                if(day!=null) {
                    wasAlarm = true;
                    setDate(day);
                    setTime(time);
                    mTv1.setVisibility(View.GONE);
                    mSp1.setVisibility(View.VISIBLE);
                    mSp2.setVisibility(View.VISIBLE);
                    mIb1.setVisibility(View.VISIBLE);
                    isAlarm = true;
                } else {
                    spinnerDate = getDate();
                    setSpinnerAdapter(mSp1,spinner1Adapter,spinnerDate);
                    spinnerTime = getTime();
                    setSpinnerAdapter(mSp2,spinner2Adapter,spinnerTime);
                }
                //Method to change ToolbarTitle when NoteTitle changed
                mName.addTextChangedListener(new TextWatcher() {

                    public void afterTextChanged(Editable s) {}

                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                    }

                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        mToolbarTitle.setText(mName.getText().toString());
                        isNoTitle();
                    }
                });
                mTv1.setOnClickListener(new TVOnClick());
                mSp1.setOnItemSelectedListener(new SpinnerInfo());
                mSp2.setOnItemSelectedListener(new SpinnerInfo());
                mIb1.setOnClickListener(new IBOnClick());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private boolean isChanged(String a, String b) {
        if(a.equals(b)) {
            return false;
        }else {
            return true;
        }
    }
    private void isNoTitle() {
        if (mName.getText().toString().matches("")) {
            mToolbarTitle.setText("My Note");
        }
    }
    public void changeNote () throws JSONException {
        //Check if anything has changed
        if(isChanged(currNote.getName(),mName.getText().toString()) ||
                isChanged(currNote.getDetail(),mDetail.getText().toString()) ||
                isAlarmChanged() || sDeleted !=0 || colorChanged || imgChanged()) {
            if(!mName.getText().toString().matches("") || !mDetail.getText().toString().matches("")) {
                //Check if note title is null
                if (mName.getText().toString().matches("")) {
                    if(mDetail.getText().toString().matches("")) {
                        mName.setText("Untitled");
                    } else {
                        if(mDetail.getLineCount()>1) {
                            mName.setText(mDetail.getText().toString().substring(0,
                                    mDetail.getText().toString().indexOf("\n")));
                        } else {
                            mName.setText(mDetail.getText().toString());
                        }
                    }
                }
            }
            mDB.deleteNote(currNote);
            if(sDeleted!=0) {
                for(int i=0;i<sDeleted;i++) {
                    imgPath.remove(sPath[i]);
                }
                sDeleted=0;
            }
            if(isAlarm) {
                getAlarm(mName.getText().toString());
                mDB.addNote(new Note(MainActivity.sId++,mName.getText().toString(),
                        mDetail.getText().toString(), mTime.getText().toString(),day,time,imgPath,mColor));
            } else {
                mDB.addNote(new Note(MainActivity.sId++,mName.getText().toString(),
                        mDetail.getText().toString(), mTime.getText().toString(),null,null,imgPath,mColor));
            }
            mDB.close();
        }
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    private List<String> getDate() {
        //Create a list for spinner
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        nextDay = String.format("Next " + new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()));
        String[] dateArray = {
                "Today",
                "Tomorrow",
                nextDay,
                "Other Day"
        };
        List<String> dateList = Arrays.asList(dateArray);
        return(dateList);
    }
    private List<String> getTime() {
        //Create a list for spinner
        String[] timeArray = {
                "6:00",
                "9:00",
                "12:00",
                "15:00",
                "19:00",
                "21:00",
                "Other Time"
        };
        List<String> timeList = Arrays.asList(timeArray);
        return(timeList);
    }
    private class TVOnClick implements View.OnClickListener {
        //Show date and time picker
        @Override
        public void onClick(View view) {
            mTv1.setVisibility(view.GONE);
            mSp1.setVisibility(view.VISIBLE);
            mSp2.setVisibility(view.VISIBLE);
            mIb1.setVisibility(view.VISIBLE);
            isAlarm = true;
        }
    }
    private class IBOnClick implements View.OnClickListener {
        //Hide date and time picker
        @Override
        public void onClick(View view) {
            mTv1.setVisibility(view.VISIBLE);
            mSp1.setVisibility(view.GONE);
            mSp2.setVisibility(view.GONE);
            mIb1.setVisibility(view.GONE);
            isAlarm = false;
        }
    }
    public class SpinnerInfo extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> spinner, View selectedView,
                                   int selectedIndex, long id) {
            String text =spinner.getItemAtPosition(selectedIndex).toString();
            switch (text) {
                case "Today":
                    break;
                case "Other Day":
                    showDatePicker(selectedView);
                    break;
                case "Other Time":
                    showTimePicker(selectedView);
                    break;
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> spinner) {
            // TODO
        }
    }
    public void setSpinnerAdapter(Spinner sp, ArrayAdapter<String> spinnerAdapter,List <String> array) {
        spinnerAdapter =
                new ArrayAdapter<String>(mContext, R.layout.spinner_item,
                        array);
        spinnerAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(spinnerAdapter);
        spinnerAdapter.notifyDataSetChanged();
    }
    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, mYear, mMonth, mDay);
        }
        public void onDateSet(DatePicker view, int year, int month, int day) {
            //set chosen date to spinner
            String selectedDay = String.format(day +"/"+ month +"/"+ year);
            setDate(selectedDay);
        }
    }

    public class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, mHour, mMinute,
                    DateFormat.is24HourFormat(getActivity()));
        }
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            //Set chosen time to spinner
            String selectedTime = String.format(hourOfDay +":"+ minute);
            if(minute<10) {
                selectedTime = String.format(hourOfDay +":0"+ minute);
            }
            setTime(selectedTime);
        }
    }
    public void showDatePicker (View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }
    public void showTimePicker(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }
    private void setDate(String d) {
        //New list for spinner
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        nextDay = String.format("Next " + new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()));
        String[] dateArray = {
                "Today",
                "Tomorrow",
                nextDay,
                d,
                "Other Day"
        };
        List<String> dateList = Arrays.asList(dateArray);
        spinnerDate=dateList;
        setSpinnerAdapter(mSp1,spinner1Adapter,spinnerDate);
        mSp1.setSelection(3);
    }
    private void setTime(String d) {
        //New list for spinner
        String[] timeArray = {
                "6:00",
                "9:00",
                "12:00",
                "15:00",
                "19:00",
                "21:00",
                d,
                "Other Time"
        };
        List<String> timeList = Arrays.asList(timeArray);
        spinnerTime=timeList;
        setSpinnerAdapter(mSp2,spinner2Adapter,spinnerTime);
        mSp2.setSelection(6);
    }
    public class ChangeColorDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            builder.setView(inflater.inflate(R.layout.dialog_choose_background_color, null));
            return builder.create();
        }
    }
    public void showChooseColorDialog() {
        newFragment = new ChangeColorDialogFragment();
        newFragment.show(getSupportFragmentManager(), "Test Title");
    }
    public void setBackgroundWhite (View v) {
        mNoteBackground.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        mColor = getResources().getColor(R.color.colorWhite);
        colorChanged = true;
        newFragment.dismiss();
    }
    public void setBackgroundOrange (View v) {
        mNoteBackground.setBackgroundColor(getResources().getColor(R.color.colorDarkenOrange));
        mColor = getResources().getColor(R.color.colorDarkenOrange);
        colorChanged = true;
        newFragment.dismiss();
    }
    public void setBackgroundGreen (View v) {
        mNoteBackground.setBackgroundColor(getResources().getColor(R.color.colorLightenGreen));
        mColor = getResources().getColor(R.color.colorLightenGreen);
        colorChanged = true;
        newFragment.dismiss();
    }
    public void setBackgroundBlue (View v) {
        final ColorPicker cp = new ColorPicker(this,0, 0, 0);
        cp.show();
        Button okColor = (Button)cp.findViewById(R.id.okColorButton);
        okColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mColor = cp.getColor();
                mNoteBackground.setBackgroundColor(mColor);
                colorChanged = true;
                cp.dismiss();
            }
        });
        newFragment.dismiss();
    }
    private void setupToolbar() {
        //for bottom toolbar
        bottomToolbar.inflateMenu(R.menu.menu_bottom);
        ImageButton btn_prev = (ImageButton) findViewById(R.id.btn_prev);
        ImageButton btn_share = (ImageButton) findViewById(R.id.btn_share);
        ImageButton btn_del = (ImageButton) findViewById(R.id.btn_delete);
        ImageButton btn_next = (ImageButton) findViewById(R.id.btn_next);
        if(mPos==(mMaxId-1)) {
            btn_next.setEnabled(false);
            btn_next.setImageDrawable(getResources().getDrawable(R.drawable.ic_next_disable));
        } else if(mPos<(mMaxId-1)) {
            btn_next.setEnabled(true);
            btn_next.setImageDrawable(getResources().getDrawable(R.drawable.btn_next));
            btn_next.setOnClickListener(new BottomToolbarOnClickListener());
        }
        if(mPos==0) {
            btn_prev.setEnabled(false);
            btn_prev.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_disable));
        } else if(mPos>0) {
            btn_prev.setEnabled(true);
            btn_prev.setImageDrawable(getResources().getDrawable(R.drawable.btn_prev));
            btn_prev.setOnClickListener(new BottomToolbarOnClickListener());
        }
        btn_share.setOnClickListener(new BottomToolbarOnClickListener());
        btn_del.setOnClickListener(new BottomToolbarOnClickListener());
    }
    private class BottomToolbarOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch(v.getId()) {
                case R.id.btn_next:
                    mPos+=1;
                    mDB.close();
                    setupInfo();
                    setupToolbar();
                    break;
                case R.id.btn_share:
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBody;
                    if(mDetail.getText().toString().matches("")){
                        shareBody = mName.getText().toString()+":";
                    } else {
                        shareBody = mName.getText().toString()+":\n"+mDetail.getText().toString();
                    }
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                    break;
                case R.id.btn_delete:
                    new AlertDialog.Builder(mContext).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Delete")
                            .setMessage("Are you sure you want to delete this note?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mDB.deleteNote(currNote);
                                    mContext.startActivity(new Intent(mContext,MainActivity.class));
                                    Intent oldIntent = new Intent(mContext, AlarmReceiver.class);
                                    PendingIntent deleteIntent = null;
                                    deleteIntent.getBroadcast(mContext, mThisId, oldIntent,
                                            PendingIntent.FLAG_CANCEL_CURRENT).cancel();
                                }
                            }).setNegativeButton("No", null).show();
                    break;
                case R.id.btn_prev:
                    mPos-=1;
                    mDB.close();
                    setupInfo();
                    setupToolbar();
                    break;
            }
        }
    }
    public boolean imgChanged() {
        if((countImage-sDeleted)>0) {
            return true;
        }
        return false;
    }
    public class InsertPictureDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            builder.setView(inflater.inflate(R.layout.dialog_picture, null));
            return builder.create();
        }
    }
    public void showInsertPictureDialog() {
        newFragment2 = new InsertPictureDialogFragment();
        newFragment2.show(getSupportFragmentManager(), "Test Title");
    }
    public void takePicture(View v) {
        dispatchTakePictureIntent();
        newFragment2.dismiss();
    }
    public void choosePicture(View v) {
        Intent i = new Intent(Intent.ACTION_PICK);
        i.setType("image/*");
        startActivityForResult(i, PICK_IMAGE);
        newFragment2.dismiss();
    }
    private void getAlarm(String a) {
        //Get alarm to save in database and pending notify
        day = mSp1.getSelectedItem().toString();
        time = mSp2.getSelectedItem().toString();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date today = new Date();
        if(day.matches("Today")) {
            day = sdf.format(today);
        } else if(day.matches("Tomorrow")) {
            Date tomorrow = new Date();
            tomorrow.setDate(today.getDate()+1);
            day = sdf.format(tomorrow);
        } else if(day.matches(nextDay)) {
            Log.d(day,"-day\n");
            Log.d(nextDay,"-nDay\n");
            Date nextweek = new Date();
            nextweek.setDate(today.getDate()+7);
            day = sdf.format(nextweek);
        }
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat alarm = new SimpleDateFormat("HH:mm dd/MM/yyyy", Locale.ENGLISH);
        try {
            cal.setTime(alarm.parse(time+" "+day));// all done
        } catch (ParseException e) {
            e.printStackTrace();
        }
        scheduleNotification(cal,a);
    }
    private boolean isAlarmChanged() {
        if(isAlarm==wasAlarm) {
            if(isAlarm) {
                day = mSp1.getSelectedItem().toString();
                time = mSp2.getSelectedItem().toString();
                if(!day.matches(currNote.getAlarmDate()) ||
                        !time.matches(currNote.getAlarmTime())) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    public void scheduleNotification(Calendar cal, String noteName) {
        Intent oldIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent deleteIntent = null;
        deleteIntent.getBroadcast(this, mThisId, oldIntent,
                PendingIntent.FLAG_CANCEL_CURRENT).cancel();
        Intent notificationIntent = new Intent(this, AlarmReceiver.class);
        notificationIntent.putExtra("id", MainActivity.sId);
        notificationIntent.putExtra("detail", noteName);
        notificationIntent.putExtra("color", mColor);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, MainActivity.sId,
                notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
    }
}
