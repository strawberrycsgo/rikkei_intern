package com.example.justakiss.mynote;
/**
 * Created by justakiss on 22/08/2016.
 * Adapter for image gridview in a note
 */
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GridImgAdap extends BaseAdapter {
    private Context context;
    private ArrayList<String> textViewValues;
    private int length;
    private int mode;

    public GridImgAdap(Context c){
        context = c;
    }
    public GridImgAdap(Context context, ArrayList<String> textViewValues) {
        this.context = context;
        this.length = textViewValues.size();
        this.textViewValues = textViewValues;
        Toast.makeText(context,"iniLength:"+length,Toast.LENGTH_LONG);
    }
    public GridImgAdap(Context context, ArrayList<String> textViewValues, int mode) {
        this.context = context;
        this.length = textViewValues.size();
        this.textViewValues = textViewValues;
        this.mode = mode;
    }
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {

            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.item_grid2, null);

        } else {
            gridView = (View) convertView;
        }
        View background = gridView.findViewById(R.id.gw_item2);
        File file = new File(textViewValues.get(position));
            Bitmap bitmap1 = ThumbnailUtils.extractThumbnail(
                    BitmapFactory.decodeFile(textViewValues.get(position)), 150,150);
            Drawable b = new BitmapDrawable(bitmap1);
            background.setBackground(b);
        Button btn_delete_pic = (Button) gridView.findViewById(R.id.btn_close);
        //Show selected picture
        gridView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // on selecting grid view image
                    // launch full screen activity
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse("file://" + textViewValues.get(position)), "image/*");
                    context.startActivity(intent);
                }
        });
        //Delete picture
        btn_delete_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // on selecting grid view image
                // launch full screen activity
                new AlertDialog.Builder(context).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Delete")
                        .setMessage("Are you sure you want to delete this picture?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(textViewValues.get(position)!=null) {
                                    File f = new File(textViewValues.get(position));
                                    f.delete();
                                    length-=1;
                                    if(mode ==1) {
                                        NewNote.sPath[NewNote.sDeleted]=textViewValues.get(position);
                                        NewNote.sDeleted+=1;
                                    } else if(mode == 2) {
                                        CurrNote.sPath[CurrNote.sDeleted]=textViewValues.get(position);
                                        CurrNote.sDeleted+=1;
                                    }
                                    notifyDataSetChanged();
                                    textViewValues.remove(position);
                                }
                            }
                        }).setNegativeButton("No", null).show();
            }
        });
        return gridView;
    }
    @Override
    public int getCount() {
        return length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}