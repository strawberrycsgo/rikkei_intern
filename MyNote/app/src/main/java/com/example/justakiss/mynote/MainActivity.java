package com.example.justakiss.mynote;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    public static int sId = 0, sFirstRun=0;
    private DBHandler mDB;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Create toolbar
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setLogo(R.drawable.ic_heartbig);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        //Creating note grid view
        GridView gridView = (GridView) findViewById(R.id.gridView1);
        mDB = new DBHandler(getApplicationContext());
        List<Note> note = mDB.getAllNotes();
        String[] array = new String[note.size()];
        String[] array2 = new String[note.size()];
        String[] array3 = new String[note.size()];
        String[] alarm = new String[note.size()];
        ArrayList<Integer> array4 = new ArrayList<Integer>();
        Integer[] color = new Integer[note.size()];
        int d,e, index = note.size()-1;
        String a, b, c;
        for (Note n : note) {
            d = n.getID();
            a = n.getName();
            b = n.getDetail();
            c = n.getDate();
            e = n.getColor();
            array[index] = a;
            array2[index] = b;
            array3[index] = c;
            array4.add(d);
            color[index] = e;
            if (n.getAlarmDate()!=null) {
                alarm[index]="yes";
            } else {
                alarm[index]="no";
            }
            index--;
        }
        gridView.setAdapter(new GridViewCustomAdapter(this, array, array2, array3, array4, color,alarm));
        mDB.close();
        //save last note id for next use
        if(sFirstRun==0) {
            SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            sId = prefs.getInt("id", 0);
            sFirstRun=1;
        } else {
            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putInt("id", sId);
            editor.commit();
            SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            sId = prefs.getInt("id", 0);
        }

    }
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                }).setNegativeButton("No", null).show();
    }
    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu1, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorite:
                Intent intent1 = new Intent(this,NewNote.class);
                this.startActivity(intent1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

