package com.example.justakiss.mynote;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DBHandler extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "notesManager";
    // Note table name
    private static final String TABLE_NOTES = "notes";
    // Note Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_DETAIL = "detail";
    private static final String KEY_DATE = "date";
    private static final String KEY_ALARMDATE = "alarm_date";
    private static final String KEY_ALARMTIME = "alarm_time";
    private static final String KEY_PHOTO = "photo";
    private static final String KEY_COLOR = "color";
    JSONObject json = new JSONObject();

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_NOTES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_DETAIL + " TEXT," + KEY_DATE + " TEXT,"
                + KEY_ALARMDATE + " TEXT," + KEY_ALARMTIME + " TEXT,"
                + KEY_PHOTO + " TEXT," + KEY_COLOR + " INTEGER" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new note
    void addNote(Note note) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        json.put("uniqueArrays", new JSONArray(note.getPhoto()));
        String arrayList = json.toString();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, note.getID());
        values.put(KEY_NAME, note.getName()); // Note Name
        values.put(KEY_DETAIL, note.getDetail()); //Note Detail
        values.put(KEY_DATE, note.getDate());// Note Date
        values.put(KEY_ALARMDATE, note.getAlarmDate());
        values.put(KEY_ALARMTIME, note.getAlarmTime());// Note Alarm
        values.put(KEY_PHOTO, arrayList);// Note Photo
        values.put(KEY_COLOR, note.getColor());
        // Inserting Row
        db.insert(TABLE_NOTES, null, values);
        db.close(); // Closing database connection
    }

    // Getting single note
    Note getNote(int id) throws JSONException {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NOTES, new String[] { KEY_ID,
                        KEY_NAME, KEY_DETAIL, KEY_DATE, KEY_ALARMDATE, KEY_ALARMTIME, KEY_PHOTO, KEY_COLOR }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        if(!cursor.moveToFirst()) {
            return null;
        }
        JSONObject json = new JSONObject(cursor.getString(6));
        JSONArray jArray = json.optJSONArray("uniqueArrays");
        ArrayList<String> arrStr = new ArrayList<String>();
        for (int i = 0; i < jArray.length(); i++) {
            arrStr.add(jArray.optString(i));
        }
        Note note = new Note(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3),
                cursor.getString(4), cursor.getString(5), arrStr, Integer.parseInt(cursor.getString(7)));
        // return contact
        return note;
    }

    // Getting All Note
    public List<Note> getAllNotes() {
        List<Note> noteList = new ArrayList<Note>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NOTES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Note note = new Note();
                note.setID(Integer.parseInt(cursor.getString(0)));
                note.setName(cursor.getString(1));
                note.setDetail(cursor.getString(2));
                note.setDate(cursor.getString(3));
                note.setAlarmDate(cursor.getString(4));
                note.setColor(Integer.parseInt(cursor.getString(7)));
                // Adding note to list
                noteList.add(note);
            } while (cursor.moveToNext());
        }

        // return note list
        return noteList;
    }

    // Updating single note
    public int updateNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, note.getName());
        values.put(KEY_DETAIL, note.getDetail());
        // updating row
        return db.update(TABLE_NOTES, values, KEY_ID + " = ?",
                new String[] { String.valueOf(note.getID()) });
    }

    // Deleting single note
    public void deleteNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NOTES, KEY_ID + " = ?",
                new String[] { String.valueOf(note.getID()) });
        db.close();
    }


    // Getting Notes Count
    public int getNotesCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NOTES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }
}