package com.example.justakiss.mynote;

/**
 * Created by justakiss on 04/08/2016.
 * Create a line in each line of note
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;
public class NoteUnderline extends EditText {
    private Paint mPaint = new Paint();
    public static NoteUnderline sEditTextDetail;
    public NoteUnderline(Context context) {
        super(context);
        initPaint();
    }
    public NoteUnderline(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }
    public NoteUnderline(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initPaint();
    }
    private void initPaint() {
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        int color = getResources().getColor(android.R.color.black);
        mPaint.setColor(color);
    }
    @Override
    protected void onDraw(Canvas canvas) {
        int right = getRight();
        int left = getLeft();
        int height = getHeight();
        int line_height = getLineHeight()-1;
        int count = height/line_height;
        if (getLineCount() > count) {
            count = getLineCount();
        }
        Paint paint = mPaint;
        int baseline = (int) (getLineHeight()-getPaddingBottom());
        for (int i = 0; i < count; i++) {
            canvas.drawLine(left, baseline, right, baseline, paint);
            canvas.drawLine(left, baseline+1, right, baseline+1, paint);
            baseline += line_height;
        }
        super.onDraw(canvas);
    }
}
