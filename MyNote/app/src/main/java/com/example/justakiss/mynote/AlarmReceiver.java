package com.example.justakiss.mynote;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by justakiss on 22/08/2016.
 * Create notify when received alarm
 */
public class AlarmReceiver extends BroadcastReceiver {
    private DBHandler mDB;
    public void onReceive(Context context, Intent intent) {
        NotificationManager notificationManager =
                (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        int id = intent.getExtras().getInt("id");
        String name = intent.getExtras().getString("detail");
        int color = intent.getExtras().getInt("color");
        mDB = new DBHandler(context);
        Notification notification = getNotification(context,id,mDB,name,color);
        notificationManager.notify(id, notification);
    }
    private Notification getNotification(Context context, int id, DBHandler mDB, String name, int color) {
        int pos =0;
        List<Note> note = mDB.getAllNotes();
        ArrayList<Integer> array = new ArrayList<Integer>();
        int d, index = note.size()-1;
        for (Note n : note) {
            d = n.getID();
            array.add(d);
            if(d==(id)) {
                pos=index;
            }
            index--;
        }
        Intent notificationIntent = new Intent(context,CurrNote.class);
        notificationIntent.putExtra("id", array);
        notificationIntent.putExtra("pos", pos);
        notificationIntent.putExtra("max", array.size());
        PendingIntent contentIntent = PendingIntent.getActivity(context,
                id,notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_heartlogo);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(getNotificationIcon())
                .setLargeIcon(bm)
                .setAutoCancel(true)
                .setContentTitle("Check This Note!!")
                .setContentText(name)
                .setColor(color);
        return builder.build();
    }
    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_heartlogo : R.drawable.ic_gallery;
    }
};
