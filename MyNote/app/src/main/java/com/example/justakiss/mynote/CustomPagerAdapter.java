package com.example.justakiss.mynote;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;

class CustomPagerAdapter extends PagerAdapter {
    private String mImgPath;
    Context mContext;
    LayoutInflater mLayoutInflater;
    private String[] FilePathStrings;
    private String[] FileNameStrings;
    private File[] listFile;
    public CustomPagerAdapter(Context context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public CustomPagerAdapter(Context context, String imgPath) {
        mContext = context;
        mImgPath = imgPath;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        File file = new File("/storage/sdcard/Android/data/com.example.justakiss.mynote/files/Pictures");
        if (file.isDirectory()) {
            listFile = file.listFiles();
            // Create a String array for FilePathStrings
            FilePathStrings = new String[listFile.length];
            // Create a String array for FileNameStrings
            FileNameStrings = new String[listFile.length];

            for (int k = 0; k < listFile.length; k++) {
                // Get the path of the image file
                FilePathStrings[k] = listFile[k].getAbsolutePath();
                // Get the name image file
                FileNameStrings[k] = listFile[k].getName();
            }
        }
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        Bitmap a = BitmapFactory.decodeFile(FilePathStrings[position]+FileNameStrings[position]);
        imageView.setImageBitmap(a);
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
