package com.example.justakiss.mynote;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


import com.pes.androidmaterialcolorpickerdialog.ColorPicker;

import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by justakiss on 03/08/2016.
 */
public class NewNote extends AppCompatActivity {
    public static int sDeleted = 0;
    public static String [] sPath = new String [100];
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int PICK_IMAGE = 2;
    private int mColor = Color.WHITE;
    private View mNoteBackground;
    private EditText mName;
    private EditText mDetail;
    private TextView mDate,mTv1;
    private Spinner mSp1, mSp2;
    private DBHandler mDB;
    private String currentDate, mCurrentPhotoPath,
            nextDay=null, day=null, time=null;
    private ImageButton mIb1;
    private TextView mToolbarTitle;
    private boolean isCreated = false, isAlarm = false;
    private int mYear,mMonth,mDay,mHour,mMinute;
    private String otherDay = "Other Day";
    private ArrayAdapter<String> spinner1Adapter, spinner2Adapter;
    private List<String> spinnerDate, spinnerTime;
    private Context mSpinnerItemContext;
    private DialogFragment newFragment, newFragment2;
    private UnscrollableGridView mGridImageView;
    private ArrayList<String> mImgPath = new ArrayList<String>();
    private int countImage = 0;
    private GridImgAdap gridImgAdap;
    private int mode = 1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        //Create toolbar
        Toolbar noteToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(noteToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //Set up new note
        mTv1 = (TextView) findViewById(R.id.tw_sp_new_note);
        mSp1 = (Spinner) findViewById(R.id.sp_date_new_note);
        mSp2 = (Spinner) findViewById(R.id.sp_time_new_note);
        mIb1 = (ImageButton) findViewById(R.id.ib_close_new);
        NoteUnderline.sEditTextDetail=(NoteUnderline) findViewById(R.id.et_new_note_detail);
        mName = (EditText) findViewById(R.id.et_new_note_title);
        mDetail = (EditText) findViewById(R.id.et_new_note_detail);
        mDate = (TextView) findViewById(R.id.tw_new_note_time);
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mNoteBackground = findViewById(R.id.newnote_id);
        mGridImageView = (UnscrollableGridView) findViewById(R.id.gridImageView);
        isNoTitle();
        //Display note create date-time
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd HH:mm");
        currentDate = sdf.format(new Date());
        mDate.setText(currentDate);
        mDB = new DBHandler(getApplicationContext());
        mTv1.setOnClickListener(new TVOnClick());
        mSpinnerItemContext = this;
        mGridImageView.setAdapter(gridImgAdap);
        spinnerDate = getDate();
        setSpinnerAdapter(mSp1,spinner1Adapter,spinnerDate);
        mSp1.setOnItemSelectedListener(new SpinnerInfo());
        spinnerTime = getTime();
        setSpinnerAdapter(mSp2,spinner2Adapter,spinnerTime);
        mSp2.setOnItemSelectedListener(new SpinnerInfo());
        mIb1.setOnClickListener(new IBOnClick());
        //Check note title change to change toolbar title
        mName.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {}
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                mToolbarTitle.setText(mName.getText().toString());
                isNoTitle();
            }
        });
        sDeleted=0;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK || requestCode == PICK_IMAGE) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                mImgPath.add(mCurrentPhotoPath);
                countImage++;
                if(sDeleted!=0) {
                    for(int i=0;i<sDeleted;i++) {
                        mImgPath.remove(sPath[i]);
                    }
                }
            }
            if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                mImgPath.add(picturePath);
                countImage++;
                if(sDeleted!=0) {
                    for(int i=0;i<sDeleted;i++) {
                        mImgPath.remove(sPath[i]);
                    }
                }
            }
            gridImgAdap = new GridImgAdap(this,mImgPath,mode);
            mGridImageView.setAdapter(gridImgAdap);
        }
    }
    @Override
    public void onBackPressed() {
        if(!isCreated) {
            try {
                addNewNote();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            isCreated = true;
        }
        Intent intent2 = new Intent(this,MainActivity.class);
        this.startActivity(intent2);
        finish();
     }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu2, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                try {
                    addNewNote();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                this.startActivity(new Intent(this,MainActivity.class));
                finish();
                return true;
            case R.id.action_add_photo:
                showInsertPictureDialog();
                return true;
            case R.id.action_confirm:
                try {
                    addNewNote();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent2 = new Intent(this,MainActivity.class);
                this.startActivity(intent2);
                finish();
                return true;
            case R.id.action_choose_color:
                showChooseColorDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private List<String> getDate() {
        //Create list for spinner
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        String nextDay = String.format("Next " + new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()));
        String[] dateArray = {
                "Today",
                "Tomorrow",
                nextDay,
                "Other Day"
        };
        List<String> dateList = Arrays.asList(dateArray);
        return(dateList);
    }
    private List<String> getTime() {
        //Create list for spinner
        String[] timeArray = {
                "6:00",
                "9:00",
                "12:00",
                "15:00",
                "19:00",
                "21:00",
                "Other Time"
        };
        List<String> timeList = Arrays.asList(timeArray);
        return(timeList);
    }
    private class TVOnClick implements View.OnClickListener {
        //Show date-time picker button
        @Override
        public void onClick(View view) {
            mTv1.setVisibility(view.GONE);
            mSp1.setVisibility(view.VISIBLE);
            mSp2.setVisibility(view.VISIBLE);
            mIb1.setVisibility(view.VISIBLE);
            isAlarm = true;
        }
    }
    private class IBOnClick implements View.OnClickListener {
        //Hide date-time picker button
        @Override
        public void onClick(View view) {
            mTv1.setVisibility(view.VISIBLE);
            mSp1.setVisibility(view.GONE);
            mSp2.setVisibility(view.GONE);
            mIb1.setVisibility(view.GONE);
            isAlarm = false;
        }
    }
    public class SpinnerInfo extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> spinner, View selectedView,
                                   int selectedIndex, long id) {
            String text =spinner.getItemAtPosition(selectedIndex).toString();
            switch (text) {
                case "Today":
                    break;
                case "Other Day":
                    showDatePicker(selectedView);
                    break;
                case "Other Time":
                    showTimePicker(selectedView);
                    break;
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> spinner) {
        // TODO
        }
    }
    private void isNoTitle() {
        if (mName.getText().toString().matches("")) {
            mToolbarTitle.setText("New Note");
        }
    }

    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, mYear, mMonth, mDay);
        }
        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            String selectedDay = String.format(day +"/"+ month +"/"+ year);
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            nextDay = String.format("Next " + new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()));
            String[] dateArray = {
                    "Today",
                    "Tomorrow",
                    nextDay,
                    selectedDay,
                    "Other Day"
            };
            //Reset spinner list
            List<String> dateList = Arrays.asList(dateArray);
            spinnerDate=dateList;
            setSpinnerAdapter(mSp1,spinner1Adapter,spinnerDate);
            mSp1.setSelection(3); // needed
        }
    }

    public class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, mHour, mMinute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Toast.makeText(getActivity(),"Time set!",
                        Toast.LENGTH_SHORT).show();
            String selectedTime = String.format(hourOfDay +":"+ minute);
            if(minute<10) {
                selectedTime = String.format(hourOfDay +":0"+ minute);
            }
            String[] timeArray = {
                    "6:00",
                    "9:00",
                    "12:00",
                    "15:00",
                    "19:00",
                    "21:00",
                    selectedTime,
                    "Other Time"
            };
            //Reset spinner list
            List<String> timeList = Arrays.asList(timeArray);
            spinnerTime=timeList;
            setSpinnerAdapter(mSp2,spinner2Adapter,spinnerTime);
            mSp2.setSelection(6); // needed
        }
    }
    public void showDatePicker (View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }
    public void showTimePicker(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }
    public void setSpinnerAdapter(Spinner sp, ArrayAdapter<String> spinnerAdapter,List <String> array) {
        spinnerAdapter =
                new ArrayAdapter<String>(mSpinnerItemContext, R.layout.spinner_item,
                        array);
        spinnerAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(spinnerAdapter);
        spinnerAdapter.notifyDataSetChanged();
    }
    public void addNewNote() throws JSONException {
        //Check if null note or not
        if (!mName.getText().toString().matches("") || !mDetail.getText().toString().matches("") ||
                isAlarm || haveImage()) {
            //Check title
            if (mName.getText().toString().matches("")) {
                if(mDetail.getText().toString().matches("")) {
                    mName.setText("Untitled");
                } else {
                    if(mDetail.getLineCount()>1) {
                        mName.setText(mDetail.getText().toString().substring(0,
                                mDetail.getText().toString().indexOf("\n")));
                    } else {
                        mName.setText(mDetail.getText().toString());
                    }
                }
            }
            //Check deleted picture
            if(sDeleted!=0) {
                for(int i=0;i<sDeleted;i++) {
                    mImgPath.remove(sPath[i]);
                }
                sDeleted=0;
            }
            //Check if alarm is set or not
            if(isAlarm) {
                getAlarm(mName.getText().toString(), mColor);
                Note newNote = new Note(MainActivity.sId++,mName.getText().toString(),
                        mDetail.getText().toString(), currentDate,day,time,mImgPath,mColor);
                mDB.addNote(newNote);
//                SharedPreferences.Editor mEditor = mPrefs.edit();
//                mEditor.putInt("id", sId).commit();
            } else {
                Note newNote = new Note(MainActivity.sId++,mName.getText().toString(),
                        mDetail.getText().toString(), currentDate,null,null,mImgPath,mColor);
                mDB.addNote(newNote);
//                SharedPreferences.Editor mEditor = mPrefs.edit();
//                mEditor.putInt("id", sId).commit();
            }
            mDB.close();
        }
    }
    public class ChangeColorDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            builder.setView(inflater.inflate(R.layout.dialog_choose_background_color, null));
            return builder.create();
        }
    }
    public class InsertPictureDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            builder.setView(inflater.inflate(R.layout.dialog_picture, null));
            return builder.create();
        }
    }
    public void showChooseColorDialog() {
        newFragment = new ChangeColorDialogFragment();
        newFragment.show(getSupportFragmentManager(), "Test Title");
    }
    public void setBackgroundWhite (View v) {
        mNoteBackground.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        mColor = getResources().getColor(R.color.colorWhite);
        newFragment.dismiss();
    }
    public void setBackgroundOrange (View v) {
        mNoteBackground.setBackgroundColor(getResources().getColor(R.color.colorDarkenOrange));
        mColor = getResources().getColor(R.color.colorDarkenOrange);
        newFragment.dismiss();
    }
    public void setBackgroundGreen (View v) {
        mNoteBackground.setBackgroundColor(getResources().getColor(R.color.colorLightenGreen));
        mColor = getResources().getColor(R.color.colorLightenGreen);
        newFragment.dismiss();
    }
    public void setBackgroundBlue (View v) {
        final ColorPicker cp = new ColorPicker(this,0, 0, 0);
        cp.show();
        Button okColor = (Button)cp.findViewById(R.id.okColorButton);
        okColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNoteBackground.setBackgroundColor(cp.getColor());
                mColor = cp.getColor();
                cp.dismiss();
            }
        });
        newFragment.dismiss();
    }
    public void showInsertPictureDialog() {
        newFragment2 = new InsertPictureDialogFragment();
        newFragment2.show(getSupportFragmentManager(), "Test Title");
    }
    public void takePicture(View v) {
        dispatchTakePictureIntent();
        newFragment2.dismiss();
    }
    public void choosePicture(View v) {
        Intent i = new Intent(Intent.ACTION_PICK);
        i.setType("image/*");
        startActivityForResult(i, PICK_IMAGE);
        newFragment2.dismiss();
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    private boolean haveImage() {
        if((countImage-sDeleted)==0) {
            return false;
        } else {
            return true;
        }
    }
    private void getAlarm(String a, int color) {
        //Get alarm to save in database and pending notify
        day = mSp1.getSelectedItem().toString();
        time = mSp2.getSelectedItem().toString();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date today = new Date();
        nextDay =    String.format("Next " + new SimpleDateFormat("EEEE", Locale.ENGLISH).format(today.getTime()));
        Log.d(day,"-day\n");
        Log.d(nextDay,"-nDay\n");
        if(day.matches("Today")) {
            day = sdf.format(today);
        } else if(day.matches("Tomorrow")) {
            Date tomorrow = new Date();
            tomorrow.setDate(today.getDate()+1);
            day = sdf.format(tomorrow);
        } else if(day.matches(nextDay)) {
            Date nextweek = new Date();
            nextweek.setDate(today.getDate()+7);
            day = sdf.format(nextweek);
        }
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat alarm = new SimpleDateFormat("HH:mm dd/MM/yyyy", Locale.ENGLISH);
        try {
            cal.setTime(alarm.parse(time+" "+day));// all done
        } catch (ParseException e) {
            e.printStackTrace();
        }
        scheduleNotification(cal,a,color);
    }
    public void scheduleNotification(Calendar cal, String noteName, int color) {
        Intent notificationIntent = new Intent(this, AlarmReceiver.class);
        notificationIntent.putExtra("id", MainActivity.sId);
        notificationIntent.putExtra("detail", noteName);
        notificationIntent.putExtra("color", color);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, MainActivity.sId, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
    }
}
